package me.beardy.ws.hello;

import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlElement;

/**
 * Class for the HelloWorldService.
 * 
 * @author Sid
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT)
public class HelloWorld {

	private static final Logger LOG = Logger.getLogger(HelloWorld.class
			.getName());

	/**
	 * Simply sends back whatever message it got.
	 * 
	 * @param str
	 *            Message to send back.
	 * 
	 * @return
	 */
	@WebMethod
	public String say(
			@XmlElement(required = true) @WebParam(name = "str") String str) {
		LOG.info("-- Method 'say' called.");
		return String.format("Processed message: %s", str);
	}

	/**
	 * Sends back whatever message it got, but in upper-case.
	 * 
	 * @param str
	 *            Message to send back.
	 * @return
	 */
	@WebMethod
	public String yell(
			@XmlElement(required = true) @WebParam(name = "str") String str) {
		LOG.info("-- Method 'yell' called.");
		return String.format("Processed message: %s", str.toUpperCase());
	}

}
