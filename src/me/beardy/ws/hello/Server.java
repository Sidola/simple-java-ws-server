package me.beardy.ws.hello;

import java.util.logging.Logger;

import javax.xml.ws.Endpoint;
import javax.xml.ws.EndpointReference;

/**
 * This is the server.
 * 
 * @author Sid
 */
public class Server {

	private static final Logger LOG = Logger.getLogger(Server.class.getName());

	private static final String HOST = "localhost";
	private static final String PORT = "9999";

	/**
	 * Start the services.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		startHelloWorld();
	}

	/**
	 * Starts the Hello World service.
	 */
	protected static void startHelloWorld() {
		HelloWorld hw = new HelloWorld();
		publish(hw);
		LOG.info("-- HelloWorld Service Started...");
	}

	/**
	 * Publishes the service.
	 * 
	 * @param service
	 *            Service to publish.
	 * @return Returns the endpoint.
	 */
	protected static Endpoint publish(Object service) {

		String url = String.format("http://%s:%s/%s", HOST,
				PORT, "hello");

		Endpoint ep = Endpoint.publish(url, service);
		EndpointReference epr = ep.getEndpointReference();
		// LOG.info("\nEndpoint Ref:\n" + epr.toString());
		return ep;
	}

}
